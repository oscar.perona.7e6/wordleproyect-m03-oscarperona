/**
 * @author Oscar Perona Gomez
 */

import java.util.*
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.text.SimpleDateFormat
import kotlin.io.path.Path
import kotlin.io.path.readLines
import kotlin.system.exitProcess

val scanner = Scanner(System.`in`)

//DECLARACION DE VARIABLES
var win: Boolean= false
var language = "Spanish"

//Colores
val green_background = "\u001B[42m"
val grey_bakcground = "\u001B[47m"
val yellow_background = "\u001B[43m"
val red_background = "\u001B[41m"
val red = "\u001B[31m"
val green = "\u001B[32m"
val black = "\u001B[30m"
val reset = "\u001B[0m"

/**
 * Funcion que se encarga de llamar al menu y atraves de este ir generando el juego
 */
fun main(){
    menu()
}

/**
 * Funcion que muestra el menu al usuario y segun la opcion que este marque realizara unas cosas u otras
 */
fun menu(){
    when(language){
        "Spanish" ->{
            do {
                println("********************")
                println("****** WORDLE ******")
                println("********************")
                println()
                println("1. Reglas")
                println("2. Jugar")
                println("3. Cambiar idioma")
                println("4. Historial")
                println("5. Salir")
                println()
                println("Introduce una opcion del menu")
                val menuOpcion = scanner.nextInt()

                when(menuOpcion){
                    1 -> {
                        println("********************")
                        println("****** REGLAS ******")
                        println("********************")
                        println()
                        println("- El jugador tendra que adivinar una palaba de 5 letras, para ello tendra  que ir introduciendo palabas de 5 letras has acertar la palabra ")
                        println("- El jugador tendra 6 vidas, por cada intento sin acierto perdera una vida, en caso de perder todas la vidas el jugador habra perdido la partida")
                        println("- Las letras de las palabras introducias tendran difernetes colores:")
                        println("             - $green_background$black VERDE $reset en caso de que la letra este en misma posicion que en la palabra a adivinar")
                        println("             - $yellow_background$black AMARILLO $reset en caso de que la letra esta en la palabra aconseguir pero no en la posicion correct")
                        println("             - $grey_bakcground$black GRIS $reset en caso de que la letra no este en la palabra")
                        repeat(4) {
                            println()
                        }
                    }
                    2 -> {
                        game()
                    }
                    3 -> {
                        language = "English"
                        menu()
                    }
                    4 -> {
                        printHitory()
                    }
                    5 -> exitProcess(0)
                    else -> {
                        println("$menuOpcion No es una opcion del menu")
                    }
                }

            }while (menuOpcion !=2)
        }
        "English" ->{
            do {
                println("********************")
                println("****** WORDLE ******")
                println("********************")
                repeat(2) {
                    println()
                }
                println("1. Rules")
                println("2. Play")
                println("3. Change language")
                println("4. Hitory")
                println("5. Exit")
                repeat(2) {
                    println()
                }
                println("Select a option from the menu")
                val menuOpcion = scanner.nextInt()

                when(menuOpcion){
                    1 -> {
                        println("*******************")
                        println("****** RULES ******")
                        println("*******************")
                        println()
                        println("- The player will have to guess a 5-letter word, for this he will have to enter 5-letter words until he guesses the word")
                        println("- The player will have 6 lives, for each unsuccessful attempt he will lose a life, in case of losing all the lives the player will have lost the game")
                        println("- The letters of the introductory words will have different colors:")
                        println("             - $green_background$black GREEN $reset in case the letter is in the same position as in the word to guess")
                        println("             - $yellow_background$black YELLOW $reset in case the letter is in the word get but not in the correct position")
                        println("             - $grey_bakcground$black GREY $reset in case the letter is not in the worda")
                        repeat(4) {
                            println()
                        }
                    }
                    2 -> {
                        game()
                    }
                    3 -> {
                        language = "Spanish"
                        menu()
                    }
                    4 -> {
                        printHitory()
                    }
                    5 -> exitProcess(0)
                    else -> {
                        println("$menuOpcion Is not a menu option")
                    }
                }

            }while (menuOpcion !=2)
        }
    }
}

/**
 * Funcion que se encarga de ralizar toda logica del juego
 */
fun game(){

    when(language){
        "Spanish" -> {
            val path = Path("txt/spanishWords")
            val randomWord = path.readLines().random()
            var lives = 6
            val repetedWords: MutableList<String> = mutableListOf("")

            println("********************")
            println("* EMPIEZA EL JUEGO *")
            println("********************")
            repeat(2) {
                println()
            }
            println("Introduzca una palabra:")
            while (lives>0 && win == false){
                val imputWord = scanner.next()
                val greenLetters: MutableList<Int> = mutableListOf()

                if (imputWord != randomWord && imputWord.length == 5 && imputWord !in repetedWords){
                    repetedWords.add(imputWord)
                    for (i in 0..4){
                        if (imputWord.get(i) == randomWord.get(i) && i !in greenLetters) {
                            print("$green_background$black ${imputWord.get(i)} $reset")
                            greenLetters.add(i)
                        }
                        else if (imputWord.get(i) in randomWord) print("$yellow_background$black ${imputWord.get(i)} $reset")
                        else print("$grey_bakcground$black ${imputWord.get(i)} $reset")
                    }
                    lives--
                    print("                              Vidas restantes = $lives")
                    println()
                }
                else if (imputWord == randomWord) win = true
                else if (imputWord.length > 5) println("La palabra tiene demasiados caracteres")
                else if (imputWord.length < 5) println("La palabra tiene caracteres insuficientes")
                else if (imputWord in repetedWords) println("La palabra ya ha sido introducida")
            }
            gameFinished(randomWord, lives, win)
        }
        "English" -> {
            val path = Path("txt/englishWords")
            val randomWord = path.readLines().random()
            var lives = 6
            val repetedWords: MutableList<String> = mutableListOf("")

            println("*******************")
            println("* THE GAME STARTS *")
            println("*******************")
            repeat(2) {
                println()
            }
            println("Enter a word:")
            while (lives>0 && win == false){
                val imputWord = scanner.next()
                val greenLetters: MutableList<Int> = mutableListOf()

                if (imputWord != randomWord && imputWord.length == 5 && imputWord !in repetedWords){
                    repetedWords.add(imputWord)
                    for (i in 0..4){
                        if (imputWord.get(i) == randomWord.get(i) && i !in greenLetters) {
                            print("$green_background$black ${imputWord.get(i)} $reset")
                            greenLetters.add(i)
                        }
                        else if (imputWord.get(i) in randomWord) print("$yellow_background$black ${imputWord.get(i)} $reset")
                        else print("$grey_bakcground$black ${imputWord.get(i)} $reset")
                    }
                    lives--
                    print("                              Remaining lives = $lives")
                    println()
                }
                else if (imputWord == randomWord) win = true
                else if (imputWord.length > 5) println("The word has too many characters")
                else if (imputWord.length < 5) println("Word has insufficient characters")
                else if (imputWord in repetedWords) println("The word has already been entered")
            }
            gameFinished(randomWord, lives, win)
        }
    }
}

/**
 * FUncion que se encarga de mostrar al usuario el resultado de la partida anterior
 */
fun gameFinished(randomWord:String, lives:Int, win:Boolean){
    when(language){
        "Spanish" -> {
            if (win == true) {
                repeat(4) {
                    println()
                }
                println("$green*****************************$reset")
                println("$green*  FELICIDADES, HAS GANADO  *$reset")
                println("$green*****************************$reset")
                repeat(2) {
                    println()
                }
                println("La palabra correcta era: $green_background$black $randomWord $reset")
                println()


            }
            else {
                repeat(4) {
                    println()
                }
                println("$red*******************************************$reset")
                println("$red*  Te has quedado sin vidas, has perdido  *$reset")
                println("$red*******************************************$reset")
                repeat(2) {
                    println()
                }
                println("La palabra correcta era: $red_background$black $randomWord $reset")
                println()
            }
        }
        "English" -> {
            if (win == true) {
                repeat(4) {
                    println()
                }
                println("$green***********************************$reset")
                println("$green*  CONGRATULATIONS, YOU HAVE WON  *$reset")
                println("$green***********************************$reset")
                repeat(2) {
                    println()
                }
                println("The correct word was: $green_background$black $randomWord $reset")
                println()


            }
            else {
                repeat(4) {
                    println()
                }
                println("$red**********************************************$reset")
                println("$red*  You have run out of lives, you have lost  *$reset")
                println("$red**********************************************$reset")
                repeat(2) {
                    println()
                }
                println("The correct word was: $red_background$black $randomWord $reset")
                println()
            }
        }
    }
    historyGestion(randomWord, lives, win)

}

/**
 * Pregunta al usuario su nombre y gestiona los datos de la ultima partida para añadirlos al historial
 */
fun historyGestion(randomWord:String, lives:Int, win:Boolean){

    val dateFormat = SimpleDateFormat("dd-MM-yyyy")
    val date = Date()
    val currentDate = dateFormat.format(date)
    val historyFile = File("history/gameHistory")
    var gameResult = ""

    when(language){
        "Spanish" -> {
            println("Por favor, introduce tu nombre de usuario:")
        }
        "English" -> {
            println("Please enter your username:")
        }
    }
    val username = readLine()
    if (win) gameResult = "WIN"
    else gameResult = "LOOSE"

    historyFile.appendText("$currentDate - $username - $gameResult attempts remaining:$lives correct word: $randomWord")
    historyFile.appendText("\n")

    newGame()
}

/**
 * Muestra al usuario un pequeño menu en el que podra elegir  que es lo siguiente que quiere hacer en el programa
 */
fun newGame(){
    when(language){
        "Spanish" -> {
            do {
                repeat(2) { println() }
                println("Que quieres hacer ahora?")
                println()
                println("1. Volver a jugar")
                println("2. Volver al menu")
                println("3. Ver hisorial")
                println("4. Salir")
                val menuOpcion = scanner.nextInt()
                when(menuOpcion){
                    1 -> game()
                    2 -> menu()
                    3 -> printHitory()
                    4 -> exitProcess(0)
                    else -> println("$menuOpcion no es una opcion del menu")
                }
            }while (menuOpcion != 1 && menuOpcion!=2)
        }
        "English" -> {
            do {
                repeat(2) { println() }
                println("What do you want to do now?")
                println()
                println("1. Play again")
                println("2. Back to menu")
                println("3. View history")
                println("4. Exit")
                val menuOpcion = scanner.nextInt()
                when(menuOpcion){
                    1 -> game()
                    2 -> menu()
                    3 -> printHitory()
                    4 -> exitProcess(0)
                    else -> println("$menuOpcion Is not a menu option")
                }
            }while (menuOpcion != 1 && menuOpcion!=2)
        }
    }
}

/**
 * Muestra por pantalla el hitorial de las partidas anteriores
 */
fun printHitory(){
    println()
    when(language){
        "Spanish" -> {
            println("*************")
            println("* HISTORIAL *")
            println("*************")
            repeat(2){ println() }
        }
        "English" -> {
            println("*************")
            println("*  HISTORY  *")
            println("*************")
            repeat(2){ println() }
        }
    }
    val historyFile = File("history/gameHistory")
    val history = historyFile.readLines()
    for (line in history){
        println(line)
    }
    repeat(2){ println() }
}
